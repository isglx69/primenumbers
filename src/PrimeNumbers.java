public class PrimeNumbers {
    public static boolean isPrime (int n) {
        int i = 2;
        boolean prime = true;
        while (i < (n / 2) && prime == true) {
            if(n % i == 0){
                prime = false;
            }
            i = i + 1;
        }
        return prime;
    }

    public static void main (String [] args) {
        int myNumber = 71;
        boolean p = isPrime(myNumber);
        System.out.println(p);
        int myNumber2 = 14;
        boolean p2 = isPrime(myNumber2);
        System.out.println(p2);
    }

}
